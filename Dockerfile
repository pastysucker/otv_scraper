FROM python

WORKDIR /usr/src/app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

ENV GUNICORN_CMD_ARGS="--bind=0.0.0.0:7575"

EXPOSE 7575

COPY ./otv_scraper ./otv_scraper
COPY ./wsgi.py ./

CMD ["gunicorn", "wsgi:app"]