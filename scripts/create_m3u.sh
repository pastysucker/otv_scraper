#!/usr/bin/env sh

RED='\e[0;31m'
BLUE='\e[0;34m'
NC='\e[0m'

log() {
    echo -e "${BLUE}[INFO]${NC} $1"
}
error() {
    echo -e "${RED}[ERROR]${NC} $1"
}

log "Sourcing venv..."
source ./.venv/bin/activate

log "Firing up the scraper..."
# Do the scraping

if ! python -m otv_scraper.tv.globalbangla -p; then
    error "Aborting because an error occurred."
    exit
fi

log "Compiling results..."
python -m otv_scraper.helpers.merge_yml
python -m otv_scraper.helpers.yml_to_m3u all.yml

log "Updating the gist endpoint..."
# Update gist on github
gist -u d991528e77a88802c805facf456ccf29 m3u/all.m3u

log "Done!"