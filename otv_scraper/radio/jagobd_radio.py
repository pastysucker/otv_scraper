from bs4 import BeautifulSoup
from urllib.parse import urlparse, parse_qs
import requests
import yaml

STREAM_URL = "https://radio.jagobd.com:444/radio/{0}/icecast.audio"

req = requests.get("http://www.jagobd.com/category/bangla-radio")

soup = BeautifulSoup(req.text, "html.parser")

channel_list = soup.select(".animage > a")

stream_urls = []

for channel_url in channel_list:
    channel_page = channel_url.attrs["href"]

    channel_req = requests.get(channel_page)

    channel_soup = BeautifulSoup(channel_req.text, "html.parser")

    stream_name = channel_soup.select_one("h2.blog-title > a").text
    player_url = channel_soup.select_one(".main-content > iframe").attrs["src"].strip()

    player_url_parsed = urlparse(player_url)

    channel_code = parse_qs(player_url_parsed.query)["u"][0]

    print(channel_code)

    stream_urls.append({"name": stream_name, "url": STREAM_URL.format(channel_code)})

with open("dump/jagobd_radio.yml", "w") as file:
    yaml.dump(stream_urls, file)
