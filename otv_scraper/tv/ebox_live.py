import re
from os import path
from urllib.parse import urlparse

import requests
import yaml
from bs4 import BeautifulSoup
from bs4.element import ResultSet

from otv_scraper.helpers.common import DUMP_DIR, get_logo

HOST = "http://tv.ebox.live"


def get_channel_name(stream_link: str):
    channel_code = urlparse(stream_link).path.split("/")[2]
    split_code = channel_code.split("_")
    if split_code[0] in ["bd", "in"]:
        _, *tail = split_code
        return " ".join(tail)
    else:
        return " ".join(split_code)


def get_stream_page(page_links: ResultSet):
    for link in page_links:
        full_url = HOST + "/" + link["href"]
        req = requests.get(full_url, headers={"referer": "http://tv.ebox.live/"})
        if not req.ok:
            continue
        else:
            yield full_url


def get_stream_link(stream_page: str):
    html = requests.get(stream_page, headers={"referer": "http://tv.ebox.live/"}).text

    stream_link: str = (
        re.findall(r"var.*?=\s*(.*?);", html, re.DOTALL | re.MULTILINE)[0]
        .replace('"', "")
        .partition("?")[0]
    )

    stream_req = requests.get(stream_link)
    if not stream_req.ok:
        return
    else:
        return stream_link


html = requests.get(HOST).text
soup = BeautifulSoup(html, "html.parser")

links = soup.find_all("a", class_="thumbnail")

channels = []

for page in get_stream_page(links):
    stream_link = get_stream_link(page)
    if stream_link:
        print(stream_link)
        stream_name = get_channel_name(stream_link)
        channels.append(
            {
                "name": stream_name,
                "url": stream_link,
                "logo": get_logo(stream_name) or "",
            }
        )

with open(path.join(DUMP_DIR, "ebox_live.yml"), "w") as CON:
    yaml.dump(channels, CON)
