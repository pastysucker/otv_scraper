import re
from os import path
import sys
import argparse

import requests
import yaml
from bs4 import BeautifulSoup

from otv_scraper.helpers.common import DUMP_DIR, IP_ADDR, get_logo, get_proper_name

HOST = "http://live.bdlan.net"
PROXY_URL = "http://{0}/bdlan?stream_page={1}"

ROGUE_CHANNELS = {
    "85": "anondo tv",
    "94": "atn bangla",
    "108": "atn news",
    "106": "bangla tv",
    "98": "desh tv",
    "109": "akash 8",
    "72": "zee bangla cinema",
    "18": "sony mix",
    "05": "zee cafe hd",
    "56": "dhoom music",
    "01": "bflix movies",
    "54": "cartoon network",
    "nicktv": "nickelodeon",
    "sony": "sony entertainment television",
    "andtv": "and tv",
    "andpic": "and pictures",
    "anBDCINEPLEX": "and flix",
}


def get_page_links(html: str):
    links = []

    soup = BeautifulSoup(html, "html.parser")

    links = soup.select("a")
    imgs = soup.select("a img")

    for link, img in zip(links, imgs):
        attr_link = link.attrs.get("onclick")
        attr_img: str = img.attrs.get("src")
        if attr_link is None or attr_img is None:
            continue

        stream_page: str = re.findall(r"href='(.*)'", attr_link, re.MULTILINE)[0]
        stream_code = attr_img.split("/")[-1].split(".")[0]
        if stream_code in ROGUE_CHANNELS:
            stream_code = ROGUE_CHANNELS[stream_code]

        links.append({"stream_page": stream_page, "stream_code": stream_code})

    return links


def get_stream_link(stream_page: str):
    try:
        html = requests.get(stream_page, headers={"referer": HOST}, timeout=4).text

        stream_link: str = re.findall(
            r"var.*?=\s*(.*?);", html, re.DOTALL | re.MULTILINE
        )[0].replace('"', "")

        stream_req = requests.get(stream_link)
        if not stream_req.ok:
            return
        else:
            return stream_link
    except requests.exceptions.RequestException:
        return


def main(proxy: bool):
    html = requests.get(HOST).text

    channels = []

    for index, link in enumerate(get_page_links(html), start=1):
        stream_page = link["stream_page"]
        stream_code = link["stream_code"]

        stream_logo = get_logo(stream_code) or ""
        stream_name = get_proper_name(stream_code)

        if not proxy:
            stream_link = get_stream_link(stream_page)

            if stream_link:
                print(
                    "#{0}: {1} | {2}".format(
                        index, stream_name, stream_link[0:40] + "..."
                    )
                )
                channels.append(
                    {
                        "name": stream_name,
                        "url": stream_link,
                        "logo": stream_logo,
                    }
                )
        else:
            proxy_link = PROXY_URL.format(IP_ADDR, stream_page)
            print(proxy_link)
            channels.append(
                {
                    "name": stream_name,
                    "url": proxy_link,
                    "logo": stream_logo,
                }
            )

    with open(path.join(DUMP_DIR, "bdlan.yml"), "w") as CON:
        yaml.dump(channels, CON)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--proxy", default=False, action="store_true")
    args = vars(parser.parse_args())

    main(args["proxy"])
