import requests
from bs4 import BeautifulSoup
import re
from otv_scraper.helpers.common import DUMP_DIR, IP_ADDR, get_logo
from os import path
import yaml
import argparse

HOST = "http://globalbanglatv.com"
PROXY_URL = "http://{0}/globalbangla?stream_page={1}"


def get_page_links(html: str):
    links = []
    soup = BeautifulSoup(html, "html.parser")

    elements = soup.select(".item-inner .title a")

    for elem in elements:
        stream_page = elem.attrs.get("href")
        stream_name = elem.text.strip()

        links.append({"stream_page": stream_page, "stream_name": stream_name})

    return links


def get_stream_link(stream_page: str):
    try:
        html = requests.get(stream_page).text
        soup = BeautifulSoup(html, "html.parser")

        script = soup.select_one(".entry-content script")

        stream_link = re.findall(r"file:\"(.+.m3u8)\"", script.string)[0]

        stream_req = requests.get(stream_link)
        if not stream_req.ok:
            return
        else:
            return stream_link

    except requests.exceptions.RequestException:
        return


def main(proxy: bool):
    html = requests.get(HOST).text

    channels = []

    for link in get_page_links(html):
        stream_page = link["stream_page"]
        stream_name = link["stream_name"]

        stream_logo = get_logo(stream_name)

        if proxy:
            proxy_link = PROXY_URL.format(IP_ADDR, stream_page)
            print(proxy_link)
            channels.append(
                {"name": stream_name, "url": proxy_link, "logo": stream_logo}
            )

    with open(path.join(DUMP_DIR, "bdlan.yml"), "w") as CON:
        yaml.dump(channels, CON)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--proxy", default=False, action="store_true")
    args = vars(parser.parse_args())

    main(args["proxy"])
