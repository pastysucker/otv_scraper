from urllib.parse import urlparse
from bs4 import BeautifulSoup
import requests
import re

HOST = urlparse("https://beta.jagobd.com")

HEADERS = {
    "User-Agent": "Mozilla/5.0 (PlayStation 4 3.11) AppleWebKit/537.73 (KHTML, like Gecko)",
    "Referer": HOST.geturl(),
    "Accept": "application/json",
    "Host": HOST.hostname,
}


def build_stream_url(stream_host: str, stream_code: str, api_key: str):
    url = "https://{0}/{1}/{2}/playlist.m3u8".format(stream_host, api_key, stream_code)
    return url


def get_channel_codes():
    channel_codes = []

    channels_list = requests.get(
        HOST.geturl() + "/api/frontEnd/populer-live-tv", headers=HEADERS
    ).json()

    for channel in channels_list:
        channel_codes.append(channel["Url"])

    return channel_codes


def get_stream_url(channel_code: str):
    req_page = requests.get(HOST.geturl() + "/" + channel_code, headers=HEADERS)

    html = req_page.text

    soup = BeautifulSoup(html, "html.parser")

    script = soup.select_one("video > source").attrs.get("src")

    print(script)


get_stream_url("somoynews")
