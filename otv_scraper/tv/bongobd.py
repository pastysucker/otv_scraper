from selenium.webdriver import Firefox
from selenium.webdriver.firefox.webelement import FirefoxWebElement
from selenium.webdriver.firefox.options import Options

opts = Options()
opts.headless = True

browser = Firefox(options=opts)
browser.get("https://bongobd.com/channels/live-tv")

channels: list[FirefoxWebElement] = browser.find_elements_by_class_name(
    "MuiCardMedia-root"
)

first_channel: FirefoxWebElement = channels[0]

first_channel.click()

elem = browser.find_element_by_tag_name("source")
print(elem.get_attribute("src"))