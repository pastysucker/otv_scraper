from os import path
import yaml

DUMP_DIR = "dump"


def merge(dir=DUMP_DIR):
    files = ["bdlan.yml", "custom.yml"]

    doc = []

    for file in files:
        with open(path.join(dir, file), "r") as con:
            contents = yaml.load(con.read(), Loader=yaml.FullLoader)
            doc += contents

    return doc


def write_to_disk(doc, dir=DUMP_DIR):
    with open(path.join(dir, "all.yml"), "w") as con:
        yaml.dump(doc, con)


write_to_disk(merge())