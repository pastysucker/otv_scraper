import os
import jellyfish
import textdistance

DUMP_DIR = "dump"
M3U_DIR = "m3u"
DATA_DIR = "data"
IP_ADDR = "192.168.0.100:7575"


def get_logo(channel_name: str):
    LOGO_URL = "https://bitbucket.org/arafatamim/otv_scraper/raw/main/logos/{0}"
    logos = [os.path.splitext(filename) for filename in os.listdir("logos")]
    first_match = sorted(
        logos,
        key=lambda x: jellyfish.jaro_distance(x[0], channel_name),
        reverse=True,
    )[0]

    original_phonetic = jellyfish.soundex(channel_name)
    match_phonetic = jellyfish.soundex(first_match[0])

    if match_phonetic == original_phonetic:
        return LOGO_URL.format("".join(first_match))
    else:
        return None


def get_proper_name(channel_code: str) -> str:
    with open(os.path.join(DATA_DIR, "channels.txt"), "r") as CON:
        content = CON.readlines()

    content = [x.strip() for x in content]

    # first pass
    first_match = sorted(
        content,
        key=lambda x: textdistance.strcmp95.distance(x, channel_code),
    )[0]

    # finally, check phonetic sounds
    original_phonetic = jellyfish.soundex(channel_code)
    match_phonetic = jellyfish.soundex(first_match)

    if original_phonetic == match_phonetic:
        return first_match
    else:
        return channel_code
