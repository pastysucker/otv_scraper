import json
import sys


def generate(input, output, group_name):
    with open("dump/{0}".format(input)) as file:
        dict = json.loads(file.read())
        with open("m3u/{0}".format(output), "w") as m3ufile:
            m3ufile.write("#EXTM3U\n\n")
            for item in dict:
                m3ufile.write(
                    '#EXTINF:-1 group-title="{0}",{1}\n'.format(
                        group_name, item["name"]
                    )
                )
                m3ufile.write(item["url"] + "\n\n")


if __name__ == "__main__":
    generate(sys.argv[1], sys.argv[2], sys.argv[3])
