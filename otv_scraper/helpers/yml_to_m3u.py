from os import path
import yaml
import sys

DUMP_DIR = "dump"
M3U_DIR = "m3u"


def generate(input):
    filename: str = path.splitext(input)[0]

    with open(path.join(DUMP_DIR, input)) as con:
        dict = yaml.load(con.read(), Loader=yaml.FullLoader)
        with open(path.join(M3U_DIR, filename + ".m3u"), "w") as m3ufile:
            m3ufile.write("#EXTM3U\n\n")
            for item in dict:
                m3ufile.write(
                    '#EXTINF:-1 tvg-logo="{0}",{1}\n'.format(
                        item.get("logo") or "", item["name"]
                    )
                )
                m3ufile.write(item["url"] + "\n\n")


if __name__ == "__main__":
    generate(sys.argv[1])
