from flask import Flask, request, Response
from otv_scraper.tv import bdlan, globalbangla

app = Flask(__name__)

MIMETYPE = "application/x-mpegURL"

M3U_LIST = """#EXTM3U
#EXT-X-VERSION:3
#EXT-X-STREAM-INF:BANDWIDTH:3264800
{0}
"""


@app.route("/")
def home():
    return "Welcome!"


@app.route("/bdlan")
def tv_bdlan():
    stream_page = request.args.get("stream_page")

    if stream_page:
        stream_link = bdlan.get_stream_link(stream_page)

        print(stream_link)

        if stream_link:
            return Response(M3U_LIST.format(stream_link), mimetype=MIMETYPE)
        else:
            return Response("Unavailable", 404)
    else:
        return "Invalid stream_page", 400


@app.route("/globalbangla")
def tv_globalbangla():
    stream_page = request.args.get("stream_page")

    if stream_page:
        stream_link = globalbangla.get_stream_link(stream_page)

        print(stream_link)

        if stream_link:
            return Response(M3U_LIST.format(stream_link), mimetype=MIMETYPE)
        else:
            return Response("Unavailable", 404)
    else:
        return "Invalid stream_page", 400


if __name__ == "__main__":
    app.run("0.0.0.0", 3000, debug=True)
